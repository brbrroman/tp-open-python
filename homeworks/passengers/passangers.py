# -*- encoding: utf-8 -*-

# Language: Python 3.5.2
# Author:   Vekshin Roman
# Date:     18.03.2018


def process(data, events, car):
    # Generate dict (car_name->train_name) from dict (train->list_of_car_names)
    def generate_cars_to_train_dict(trains):
        cars_to_train_dict = {}
        for train_name in trains:
            for car_name in trains[train_name]:
                cars_to_train_dict[car_name] = train_name
        return cars_to_train_dict

    # Count how many people are in the car
    def how_much(people_dict, car_name):
        count = 0
        for people_name in people_dict:
            if people_dict[people_name] == car_name:
                count += 1
        return count

    # Generate dict (train->list_of_car_names) and dict (people->car_name)
    # from data_list
    def parse_data(data):
        trains_dict = {}
        people_dict = {}
        for data_unit in data:
            trains_dict[data_unit['name']] = []
            for cars_list in data_unit['cars']:
                trains_dict[data_unit['name']].append(cars_list['name'])
                for person_name in cars_list['people']:
                    people_dict[person_name] = cars_list['name']
        return (trains_dict, people_dict)

    # --- Function main body ---

    (trains_dict, people_dict) = parse_data(data)

    cars_to_train_dict = generate_cars_to_train_dict(trains_dict)
    for event in events:
        if event['type'] == 'walk':
            if event['passenger'] in people_dict.keys():
                _car = people_dict[event['passenger']]
                steps = event['distance']
                _train = trains_dict[cars_to_train_dict[_car]]
                if 0 <= steps + _train.index(_car) <= (len(_train) - 1):
                    people_dict[event['passenger']] = \
                        _train[steps + _train.index(_car)]
                else:  # step check
                    return -1
            else:  # passenger check
                return -1
        elif event['type'] == 'switch':  # event check
            if (event['train_from'] in trains_dict.keys()) and \
                    (event['train_to'] in trains_dict.keys()):
                if 0 <= event['cars'] <= len(trains_dict[event['train_from']]):
                    trains_dict[event['train_to']] += \
                        trains_dict[event['train_from']][-event['cars']:]
                    del trains_dict[event['train_from']][-event['cars']:]
                    cars_to_train_dict = generate_cars_to_train_dict(
                        trains_dict)
                else:  # cars check
                    return -1
            else: # trains check
                return -1
        else:  # event check
            return -1

    return how_much(people_dict, car)
