# -*- encoding: utf-8 -*-

# Language: Python 3.5.2
# Author:   Vekshin Roman
# Date:     26.04.2018

from datetime import datetime
from types import FunctionType
from functools import wraps

def profile(obj):
    if isinstance(obj, FunctionType):
        @wraps(obj)
        def wrapper(*args, **kwargs):
            _time_start = datetime.now()
            print("\'%s\' started" % obj.__qualname__)
            result = obj(*args, **kwargs)
            _time_stop = datetime.now()
            print("\'%s\' finished in %fs" % (obj.__qualname__, (_time_stop - _time_start).total_seconds()))
            return result
        return wrapper
    else:
        for attr_name in obj.__dict__:
            attr_body = getattr(obj, attr_name)
            if isinstance(attr_body, FunctionType):
                setattr(obj, attr_name, profile(attr_body))
        return obj
