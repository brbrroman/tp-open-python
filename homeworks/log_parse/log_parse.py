# -*- encoding: utf-8 -*-

# Language: Python 3.5.2
# Author:   Vekshin Roman
# Date:     24.03.2018

from datetime import datetime
from urllib.parse import urlparse
from re import match
from collections import defaultdict


# Разбор строки в словарь или None
def str_to_dict(pattern, string):
    result = match(pattern, string)
    return result.groupdict() if result else None


# Проверка нахождения числа в интервале, если границы интервала существуют
def value_in_interval(val, start, stop):
    return ((start is None) or (start <= val)) \
        and ((stop is None) or (val <= stop))


# Паттерн лога
def get_log_pattern(request_type):
    return (
        # Дата лога
        '\[(?P<date>.*)\] '
        # Тип запроса
        '"(?P<method>' + \
        (request_type if request_type else '[A-Z]+') + ') '
        # URL
        '(?P<url>[^\s]+) '
        # Протокол и его версия (если есть)
        '(?P<protocol>[A-Z]+\/\d\.\d)?" '
        # Код ответа
        '(?P<status>\d+) '
        # Время выполнения запроса
        '(?P<t_time>\d+)'
    )


# Паттерн хоста
def get_host_pattern(ignore_www):
    return (
        # www. (если есть)
        '' + ('(?P<www_part>[w]{3}\.)?' if ignore_www else '') + ''
        # Хост без www.
        '(?P<host>[^\/\:]+)'
    )


# Условие для сортировки медленных запросов
def slow(x):
    return (sum(x) // len(x))


# Условие для сортировки запросов по количеству
def not_slow(x):
    return len(x)


# Вернуть топ 5 (медленных?) запросов
def find_top_5(url_dict, slow_queries):
    pre_result = url_dict.values()
    if slow_queries:
        return list(map(slow, sorted(pre_result, key=slow, reverse=True)[:5]))
    else:
        return list(map(not_slow, sorted(pre_result, key=not_slow, reverse=True)[:5]))


# Вернуть топ 5 (медленных?) запросов из логов с параметрами
def parse(
    ignore_files=False,  # Игнорировать файлы?
    ignore_urls=[],     # Игнорировать urls из списка
    start_at=None,      # Рассматривать логи с этой даты
    stop_at=None,       # Рассматривать логи до этой даты
    request_type=None,  # Тип запроса GET | POST | PUT ...
    ignore_www=False,   # Игнорировать www перед доменом?
    slow_queries=False  # Возвращать топ медленных запросов
):
    count_dict = defaultdict(list)
    with open('log.log') as log_file:
        for log_string in log_file:
            log_dict = str_to_dict(get_log_pattern(request_type), log_string)
            # Если строка разобралась по паттерну
            if log_dict:
                try:
                    date = datetime.strptime(
                        log_dict['date'], '%d/%b/%Y %H:%M:%S')
                except ValueError:
                    return []
                # Если дата валидна
                if value_in_interval(date, start_at, stop_at):
                    url_dict = urlparse(log_dict['url'])
                    # Разбор хоста по паттерну с учетом условия для www.
                    host = str_to_dict(get_host_pattern(
                        ignore_www), url_dict.hostname)['host']
                    # Полный путь
                    full_url = host + url_dict.path
                    if full_url not in ignore_urls:
                        if not (
                            (full_url.rfind('\/') <= full_url.rfind('.') < -1)
                            and ignore_files
                        ):
                            count_dict.setdefault(full_url, []).append(
                                int(log_dict['t_time']))
    return find_top_5(count_dict, slow_queries)
