# -*- encoding: utf-8 -*-

# Language: Python 3.5.2
# Author:   Vekshin Roman
# Date:     04.05.2018

from unittest import TestCase
from DirDict import DirDict
import os
import shutil

class DirDictTestCase(TestCase):
    def setUp(self):
        self.path = os.path.join(os.getcwd(), '.temp')
        print(self.path)
        if os.path.exists(self.path):
            shutil.rmtree(self.path)
        os.mkdir(self.path)
        self.tmp = DirDict(self.path)
        self.tmp['0'] = '0'
        self.tmp['1'] = '1'
        self.tmp['2'] = '2'
        self.tmp['3'] = '3'
        
    def test__init__(self):
        pass

    def test__getitem__(self):
        with open(self.path + '/test', 'w') as element:
            element.write('ok')
        self.assertEqual('ok', self.tmp['test'], 'Ошибка чтения элемента словаря')
        
    def test__setitem__(self):
        self.tmp['5'] = 'ok'
        if os.path.isfile(self.path + '/5'):
            with open(self.path + '/5', 'r') as element:
                self.assertEqual('ok', element.read(), 'Ошибка установки элемента словаря')
        
    def test__delitem__(self):
        del self.tmp['0']
        self.assertEqual(3, len(self.tmp), 'Ошибка удаление элемента словаря')
        pass
        
    def test__iter__(self):
        pass
        
    def test__len__(self):
        self.assertEqual(4, len(self.tmp), 'Ошибка вычисления размера словаря')
