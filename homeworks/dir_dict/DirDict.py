# -*- encoding: utf-8 -*-

# Language: Python 3.5.2
# Author:   Vekshin Roman
# Date:     25.04.2018

import os
import shutil
from collections.abc import MutableMapping


class DirDict(MutableMapping):
    def __init__(self, path):
        self._path = path
        if not os.path.exists(self._path):
            raise IOError

    def __getitem__(self, key):
        if os.path.isfile(self._path + '/' + str(key)):
            with open(self._path + '/' + str(key), 'r') as element:
                return element.read()
        else:
            raise KeyError

    def __setitem__(self, key, value):
        with open(self._path + '/' + str(key), 'w') as element:
            element.write(value)

    def __delitem__(self, key):
        if os.path.isfile(self._path + '/' + str(key)):
            os.remove(self._path + '/' + str(key))
        else:
            raise KeyError

    def __iter__(self):
        for f in [el for el in os.listdir(self._path) if os.path.isfile(self._path + '/' + el)]:
            yield (f, self[f])

    def __len__(self):
        return len([el for el in self])
    
    def clear(self):
        shutil.rmtree(self._path)
        os.mkdir(self._path)
